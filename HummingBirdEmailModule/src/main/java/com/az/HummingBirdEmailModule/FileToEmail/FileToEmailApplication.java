package com.az.HummingBirdEmailModule.FileToEmail;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by kjfg254 on 5/16/2017.
 */

public class FileToEmailApplication {
    private static Logger logger = Logger.getLogger(FileToEmailApplication.class.getName());
    public static void main(String[] args) throws IOException {
        Long starttime=System.currentTimeMillis();
        String jobname="HummingBirdEmailModule";

        if (args.length < 7) {
            System.out.println("At least 7 argument need to be passed");
            System.err.println("Usage:"+ "\n" +
                    "Source File           :- /AZ/hummingbirdemailmodule/{your_source_file}.txt {YOUR_SOURCE_FILE}" + "\n" +
                    "FromEamil             :- HB_ops@astrazeneca.com {YOUR_TO_ADDRESS}" + "\n" +
                    "Outlookusername       :- sri.tummala@astrazeneca.com {YOUR_AZ_EMAIL_ID}"+ "\n" +
                    "Outlookspassword      :- Password1 {YOUR_WINDOWS_LAPTOP_PASSWORD}" + "\n" +
                    "Bcc reciept           :- sri.tummala@astrazenenca.com,selvaraj.rajabhathor@astrazeneca.com {YOUR_EMAIL_BCC} " + "\n" +
                    "Subject               :- Sync Veeva for latest suggestions {YOUR_EMAIL_SUBJECT}" + "\n" +
                    "Source File Delimeter :- ~ {YOUR_DELIMETER}");
            System.exit(1);
        }

        if (args.length >= 7) {
            System.out.println("Argument 1:Source File            :- "+args[0]);
            System.out.println("Argument 2:FromEamil              :- "+args[1]);
            System.out.println("Argument 3:Outlookusername        :- "+args[2]);
            System.out.println("Argument 4:Outlookspassword       :- "+args[3]);
            System.out.println("Argument 5:Bcc reciept            :- "+args[4]);
            System.out.println("Argument 6:Subject                :- "+args[5]);
            System.out.println("Argument 7:Source File Delimeter  :- "+args[6]);
        }

        String inputfilename=args[0];
        String appfromemail=args[1];
        String appusername=args[2];
        String apppassword=args[3];
        String bccvalue=args[4];
        String subjectvalue=args[5];
        String sourcefiledelimeter=args[6];

        System.out.println("Argument values passed to variables");
        System.out.println("Source File            :- "+inputfilename);
        System.out.println("FromEamil              :- "+appfromemail);
        System.out.println("Outlookusername        :- "+appusername);
        System.out.println("Outlookspassword       :- "+apppassword);
        System.out.println("Bcc reciept            :- "+bccvalue);
        System.out.println("Subject                :- "+subjectvalue);
        System.out.println("Source File Delimeter  :- "+sourcefiledelimeter);

        FileInputStream fis = null;

        try{

            fis = new FileInputStream(inputfilename);
            DataInputStream myInput = new DataInputStream(fis);
            String currentLine;
            ArrayList<String> oneRowData = null;
            ArrayList<ArrayList<String>> allRowAndColData = null;

            while ((currentLine = myInput.readLine()) != null) {
                String[] temp;
                temp=currentLine.split(sourcefiledelimeter);
                if (temp[0].isEmpty())
                {
                    System.out.println("Bad Record Line");
                    System.out.println(currentLine);
                }
                else {
                    String emailid = temp[0].replaceAll("\"", "");
                    String htmltag = temp[1].replaceAll("\"", "");
                    System.out.println(emailid);
                    System.out.println(htmltag);

                    Email sendemailattachmenttest=new Email();
                    int emailop=sendemailattachmenttest.sendemailmethod(emailid,appfromemail,appusername,apppassword,htmltag,bccvalue,subjectvalue);
                    if (emailop==0) {
                        System.out.println("Email Sent for user :-"+emailid);
                    }
                }
            }

            Long endTime=System.currentTimeMillis();
            Long totalTime=endTime-starttime;
            System.out.println("JobName "+ jobname +" took ("+(totalTime/1000d) + ") seconds to process ");

        }
        catch (FileNotFoundException e){
            e.printStackTrace();
        } finally {
            try {
                if (fis != null)
                    fis.close();
            } catch (IOException ex){
                ex.printStackTrace();
            }
        }
    }
}
