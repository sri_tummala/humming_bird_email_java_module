package com.az.HummingBirdEmailModule.FileToEmail;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.Properties;

/**
 * Created by kjfg254 on 5/16/2017.
 */
public class Email {
    public int sendemailmethod(String apptoemail,String appfromemail,String appusername,String apppassword,String apphtml,String bcc,String subjectvalue){

        //reciver email address
        String to=apptoemail;

        //senders email address
        String from=appfromemail;

        //change accordingly
        final String username = appusername;

        //change accordingly
        final String password = apppassword;

        Properties props = new Properties();
        props.put("mail.smtp.auth", true);
        props.put("mail.smtp.starttls.enable", true);
        props.put("mail.smtp.host", "relay.astrazeneca.net");
        props.put("mail.smtp.port", "25");
        props.put("mail.debug", "true");

        // Get the Session object.
        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {

            // Create a default MimeMessage object.
            Message message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(from));

            String recipient = bcc;
            String[] recipientList = recipient.split(",");
            InternetAddress[] recipientAddress = new InternetAddress[recipientList.length];
            int counter = 0;
            for (String recipienttmp : recipientList) {
                recipientAddress[counter] = new InternetAddress(recipienttmp.trim());
                counter++;
            }
            message.setRecipients(Message.RecipientType.BCC, recipientAddress);

            // Set To: header field of the header.
            message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(to));

            // Set Subject: header field
            message.setSubject(subjectvalue);

            // Create the message part
            BodyPart messageBodyPart = new MimeBodyPart();

            String emailcontent=apphtml;
            messageBodyPart.setContent(emailcontent, "text/html; charset=utf-8");

            // Create a multipar message
            Multipart multipart = new MimeMultipart();
            // Set text message part
            multipart.addBodyPart(messageBodyPart);

            //multipart.addBodyPart(messageBodyPart);

            message.setContent(multipart);
            // Send message
            Transport.send(message);

            System.out.println("Sent message successfully....");

        } catch (MessagingException e){
            throw new RuntimeException(e);
        }
        return 0;

    }

}
